package br.com.desafio.twitter.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;

@SpringBootTest
public class ConverteJsonTest {
	
	@Test
	public void converteDados() throws JsonMappingException, JsonProcessingException {

		String dados = "{\"teste\":\"teste\"}";
		
		String valor = "teste";
		
		ConverteJson converte = new ConverteJson();
		JsonNode node = converte.converterDados(dados);
		
		assertEquals(valor, node.path("teste").asText());
		
	}
}
