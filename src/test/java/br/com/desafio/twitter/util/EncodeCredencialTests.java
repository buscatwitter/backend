package br.com.desafio.twitter.util;

import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class EncodeCredencialTests {
	
	@Test
	public void testEncodeDadosCredencial() {
		
		String key = "kin5e9GyHSvWW26w0CCCCCCCC";
		String secret = "Ncc53dBGaJG22PP9iJKQJj7FCEbiItd1FMaBio4mlhhUn2AAAA";
		
		String credencial = key + ":" + secret;
		
		EncodeCredencial encode = new EncodeCredencial();
		String retorno = encode.encodeDadosCredencial(key, secret);
		
		assertNotEquals(credencial, retorno);
	}
}
