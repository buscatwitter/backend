package br.com.desafio.twitter.util;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.desafio.twitter.dto.DadosTweetDTO;
import br.com.desafio.twitter.interfaces.DadosTweetRepository;
import br.com.desafio.twitter.model.DadosTweet;

@Service
public class RespostaControllerHashtag {

	@Autowired
	DadosTweetRepository repository;

	public DadosTweetDTO montaRespostaHashtag(String hashtag) {

		DadosTweetDTO dadosTweetDTO = new DadosTweetDTO();

		if (hashtag.isEmpty()) {
			dadosTweetDTO.setHashtag("Pesquisa Vazia");
			dadosTweetDTO.setQtd_tweets(0);
		} else {
			List<DadosTweet> lista = repository.findByHashtag(hashtag);
			dadosTweetDTO.setQtd_tweets(lista.size());
			dadosTweetDTO.setHashtag(hashtag);
		}

		return dadosTweetDTO;

	}

}
