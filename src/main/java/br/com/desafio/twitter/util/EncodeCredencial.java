package br.com.desafio.twitter.util;

import java.util.Base64;

import org.springframework.stereotype.Component;

@Component
public class EncodeCredencial {
	
	//Metodo que converte para Base64 as credenciais
	
	public String encodeDadosCredencial(String key, String secret) {
				
		String credencial = key + ":" + secret;
		String credencialEncoded = new String(Base64.getEncoder().encodeToString(credencial.getBytes()));

		return credencialEncoded ;
		
	}
}
