package br.com.desafio.twitter.interfaces;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.desafio.twitter.model.DadosSeguidores;

public interface DadosSeguidoresRepository extends MongoRepository<DadosSeguidores, String> {

	void deleteByHashtag(String hashtag);
	
	

}
