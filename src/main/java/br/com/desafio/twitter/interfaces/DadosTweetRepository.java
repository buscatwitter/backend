package br.com.desafio.twitter.interfaces;

import java.util.List;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.desafio.twitter.model.DadosTweet;

public interface DadosTweetRepository extends MongoRepository<DadosTweet, String> {

	DadosTweet findBy_id(ObjectId _id);

	List<DadosTweet> findByHashtag(String hashtag);

	void deleteByHashtag(String hashtag);

}
