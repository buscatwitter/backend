package br.com.desafio.twitter.interfaces;

import java.util.List;

import br.com.desafio.twitter.model.DadosTweet;

public interface DadosTweetRepositoryCustom {
	
	List<DadosTweet> maisSeguidores(String hashtag);

}
