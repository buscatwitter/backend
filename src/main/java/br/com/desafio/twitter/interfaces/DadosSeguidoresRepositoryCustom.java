package br.com.desafio.twitter.interfaces;

import java.util.List;

import br.com.desafio.twitter.model.DadosSeguidores;

public interface DadosSeguidoresRepositoryCustom {
	
	List<DadosSeguidores> topSeguidores();

}
