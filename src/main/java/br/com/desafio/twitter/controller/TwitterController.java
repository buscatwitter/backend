package br.com.desafio.twitter.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import br.com.desafio.twitter.dto.DadosTweetDTO;
import br.com.desafio.twitter.dto.DadosSeguidoresDTO;
import br.com.desafio.twitter.form.DadosTweetForm;
import br.com.desafio.twitter.interfaces.DadosSeguidoresRepository;
import br.com.desafio.twitter.interfaces.DadosSeguidoresRepositoryCustom;
import br.com.desafio.twitter.interfaces.DadosTweetRepository;
import br.com.desafio.twitter.interfaces.DadosTweetRepositoryCustom;
import br.com.desafio.twitter.service.ColetaTweets;
import br.com.desafio.twitter.service.GeraTokenAutenticacao;
import br.com.desafio.twitter.util.RespostaControllerHashtag;

@RestController
@RequestMapping("/twitter/v1/")
public class TwitterController {

	@Autowired
	DadosTweetRepository repository;

	@Autowired
	DadosTweetRepositoryCustom repositoryCustom;

	@Autowired
	GeraTokenAutenticacao pegaToken;

	@Autowired
	ColetaTweets coletaTweets;

	@Autowired
	DadosSeguidoresRepositoryCustom repositorySeguidoresCustom;

	@Autowired
	RespostaControllerHashtag respostaControllerHashtag;

	@Autowired
	DadosSeguidoresRepository repositoryDadosSeguidores;

	// Realiza A Pesquisa de Hashtag e insere dados no banco
	@CrossOrigin
	@CacheEvict(value = "listaUsuarios", allEntries = true)
	@RequestMapping(value = "/hashtag", method = RequestMethod.POST)
	public DadosTweetDTO getAllTwitts(@RequestBody DadosTweetForm dadosTweet)
			throws JsonMappingException, JsonProcessingException {

		DadosTweetDTO dto_resp = new DadosTweetDTO();

		if (dadosTweet.getHashtag().isEmpty()) {

			dto_resp = respostaControllerHashtag.montaRespostaHashtag(dadosTweet.getHashtag());

		} else {
			coletaTweets.coletaTweet(dadosTweet.getHashtag());
			dto_resp = respostaControllerHashtag.montaRespostaHashtag(dadosTweet.getHashtag());
		}

		return dto_resp;
	}

	// Realiza A Pesquisa e Retorna os Top 5 usuarios com mais seguidores
	@CrossOrigin
	@Cacheable(value = "listaUsuarios")
	@RequestMapping(value = "/seguidores", method = RequestMethod.GET)
	public List<DadosSeguidoresDTO> maisSeguidores() {

		return DadosSeguidoresDTO.converter(repositorySeguidoresCustom.topSeguidores());
	}
}
