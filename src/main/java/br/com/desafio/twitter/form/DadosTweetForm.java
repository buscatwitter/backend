package br.com.desafio.twitter.form;

public class DadosTweetForm {
	
	private String hashtag;

	public String getHashtag() {
		return hashtag;
	}

	public void setHashtag(String hashtag) {
		this.hashtag = hashtag;
	}
}
