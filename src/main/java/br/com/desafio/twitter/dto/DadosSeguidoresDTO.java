package br.com.desafio.twitter.dto;

import java.util.List;
import java.util.stream.Collectors;
import br.com.desafio.twitter.model.DadosSeguidores;

public class DadosSeguidoresDTO {

	private String hashtag;
	private String user_name;
	private int user_followers_count;
	private String user_profile_image_url;

	public DadosSeguidoresDTO(DadosSeguidores dadosSeguidores) {
		this.hashtag = dadosSeguidores.getHashtag();
		this.user_name = dadosSeguidores.getUser_name();
		this.user_followers_count = dadosSeguidores.getUser_followers_count();
		this.user_profile_image_url = dadosSeguidores.getUser_profile_image_url();

	}

	public String getHashtag() {
		return hashtag;
	}

	public String getUser_name() {
		return user_name;
	}

	public int getUser_followers_count() {
		return user_followers_count;
	}

	public String getUser_profile_image_url() {
		return user_profile_image_url;
	}

	public static List<DadosSeguidoresDTO> converter(List<DadosSeguidores> dadosTweet) {

		return dadosTweet.stream().map(DadosSeguidoresDTO::new).collect(Collectors.toList());

	}

}
