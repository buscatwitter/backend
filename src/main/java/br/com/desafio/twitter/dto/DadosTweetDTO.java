package br.com.desafio.twitter.dto;

public class DadosTweetDTO {

	private String hashtag;
	private int qtd_tweets;
	
	public String getHashtag() {
		return hashtag;
	}
	public void setHashtag(String hashtag) {
		this.hashtag = hashtag;
	}
	public int getQtd_tweets() {
		return qtd_tweets;
	}
	public void setQtd_tweets(int qtd_tweets) {
		this.qtd_tweets = qtd_tweets;
	}
	
	
}
