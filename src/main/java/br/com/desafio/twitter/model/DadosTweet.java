package br.com.desafio.twitter.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "tweet")
public class DadosTweet {

	@Id
	private ObjectId _id;
	private String hashtag;
	private String created_at;
	private String hashtag_id;
	private String user_id;
	private String user_name;
	private int user_followers_count;
	private String user_profile_image_url;
	private String text;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public DadosTweet() {
	};

	public DadosTweet(ObjectId _id, String hashtag, String hashtag_id, String user_id, String user_name,
			int user_followers_count, String user_profile_image_url, String text) {
		super();
		this._id = _id;
		this.hashtag = hashtag;
		this.hashtag_id = hashtag_id;
		this.user_id = user_id;
		this.user_name = user_name;
		this.user_followers_count = user_followers_count;
		this.user_profile_image_url = user_profile_image_url;
		this.text = text;
	}

	public ObjectId get_id() {
		return _id;
	}

	public void set_id(ObjectId _id) {
		this._id = _id;
	}

	public String getHashtag() {
		return hashtag;
	}

	public void setHashtag(String hashtag) {
		this.hashtag = hashtag;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getHashtag_id() {
		return hashtag_id;
	}

	public void setHashtag_id(String hashtag_id) {
		this.hashtag_id = hashtag_id;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public int getUser_followers_count() {
		return user_followers_count;
	}

	public void setUser_followers_count(int user_followers_count) {
		this.user_followers_count = user_followers_count;
	}

	public String getUser_profile_image_url() {
		return user_profile_image_url;
	}

	public void setUser_profile_image_url(String user_profile_image_url) {
		this.user_profile_image_url = user_profile_image_url;
	}

}
