package br.com.desafio.twitter.model;

public class DadosSeguidores {

	private String hashtag;
	private String user_name;
	private int user_followers_count;
	private String user_profile_image_url;

	public String getHashtag() {
		return hashtag;
	}

	public void setHashtag(String hashtag) {
		this.hashtag = hashtag;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public int getUser_followers_count() {
		return user_followers_count;
	}

	public void setUser_followers_count(int user_followers_count) {
		this.user_followers_count = user_followers_count;
	}

	public String getUser_profile_image_url() {
		return user_profile_image_url;
	}

	public void setUser_profile_image_url(String user_profile_image_url) {
		this.user_profile_image_url = user_profile_image_url;
	}

}
