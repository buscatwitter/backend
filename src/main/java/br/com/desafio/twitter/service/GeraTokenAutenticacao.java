package br.com.desafio.twitter.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import br.com.desafio.twitter.components.DadosCredencial;
import br.com.desafio.twitter.dto.DadosTokenDTO;
import br.com.desafio.twitter.util.EncodeCredencial;

@Service
public class GeraTokenAutenticacao {
	
	@Autowired
	private DadosCredencial credencial;
	
	@Autowired
	private EncodeCredencial encode;
	
	public ResponseEntity<DadosTokenDTO> geraToken(){
		
		// Monta a URL
		String url = "https://api.twitter.com".concat(credencial.getUrl());
		
		// Credencial
		String encodedCredencial = encode.encodeDadosCredencial(credencial.getKey(), credencial.getSecret());
		
		//Rest Template
		RestTemplate restTemplate = new RestTemplate();
		
		//Inclusao do Header
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		headers.setContentLength(29);
		headers.add("Authorization", "Basic " + encodedCredencial);
		
		//Inclusao do Body
		MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
		body.add("grant_type", "client_credentials");	
		
		//Construindo a Request
		HttpEntity<?> request = new HttpEntity<Object>(body, headers);
		
		//Faz a Chamada e Guarda a Resposta
		ResponseEntity<DadosTokenDTO> response = restTemplate.exchange(
		        url,
		        HttpMethod.POST,
		        request,
		        DadosTokenDTO.class
		);	
		
		return response;
	}
}