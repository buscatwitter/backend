package br.com.desafio.twitter.service;

import java.nio.charset.StandardCharsets;
import java.util.List;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import br.com.desafio.twitter.components.DadosColetaTweet;
import br.com.desafio.twitter.interfaces.DadosSeguidoresRepository;
import br.com.desafio.twitter.interfaces.DadosTweetRepository;
import br.com.desafio.twitter.model.DadosTweet;
import br.com.desafio.twitter.util.ConverteJson;

@Service
@Transactional
public class ColetaTweets {

	@Autowired
	private DadosColetaTweet dadosColeta;

	@Autowired
	private GeraTokenAutenticacao Token;

	@Autowired
	private ConverteJson converteJson;

	@Autowired
	private DadosTweetRepository dadosTweetRepository;

	@Autowired
	private DadosSeguidoresRepository dadosSeguidoresRepository;

	public void coletaTweet(String hashtag) throws JsonMappingException, JsonProcessingException {

		// Monta URL
		String url = "https://api.twitter.com".concat(dadosColeta.getUrl());

		// Rest Template
		RestTemplate restTemplate = new RestTemplate();

		// Inclusao do Header
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization",
				Token.geraToken().getBody().getToken_type() + " " + Token.geraToken().getBody().getAccess_token());

		// Inclusao dos Parametros para consulta
		UriComponents builder = UriComponentsBuilder.fromHttpUrl(url).queryParam("q", hashtag)
				.queryParam("include_entities", "true").queryParam("count", 100).encode(StandardCharsets.UTF_8).build();

		// Construindo a Request
		HttpEntity<?> request = new HttpEntity<Object>(headers);

		// Faz a Chamada e Guarda a Resposta
		ResponseEntity<String> response = restTemplate.exchange(builder.toUri(), HttpMethod.GET, request, String.class);

		// Converte Resposta
		String dados_resp = response.getBody().toString();
		JsonNode json = converteJson.converterDados(dados_resp);

		// Acessa Item Raiz do Json
		JsonNode root = json.path("statuses");

		// Verifica se ja existe a hashtag no banco
		List<DadosTweet> tweet_hashtag = dadosTweetRepository.findByHashtag(hashtag);

		if (root.isArray()) {

			// Caso a hashtag ja exista no banco, os dados sao limpos
			if (tweet_hashtag.isEmpty() == false) {
				dadosTweetRepository.deleteByHashtag(hashtag);
				dadosSeguidoresRepository.deleteByHashtag(hashtag);
			}

			// Novos dados sao enviados
			for (JsonNode node : root) {

				DadosTweet dados = new DadosTweet();

				dados.set_id(ObjectId.get());
				dados.setHashtag(hashtag);
				dados.setCreated_at(node.path("created_at").asText());
				dados.setHashtag_id(node.path("id").asText());
				dados.setUser_id(node.path("id").asText());
				dados.setUser_name(node.path("user").path("name").asText());
				dados.setUser_followers_count(node.path("user").path("followers_count").asInt());
				dados.setUser_profile_image_url(node.path("user").path("profile_image_url_https").asText());
				dados.setText(node.path("text").asText());
				dadosTweetRepository.save(dados);
			}

		}

	}

}
