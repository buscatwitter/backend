package br.com.desafio.twitter.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;


import br.com.desafio.twitter.interfaces.DadosTweetRepositoryCustom;
import br.com.desafio.twitter.model.DadosTweet;

@Service
public class DadosTweetRepositoryCustomImplements implements DadosTweetRepositoryCustom {

	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public List<DadosTweet> maisSeguidores(String hashtag) {
		
		
		final Query query = new Query();
		
		//Query que Identifica o Usuario com mais Seguidores de uma determinada hashtag
		query.addCriteria(Criteria.where("hashtag").is(hashtag)).limit(1).with(Sort.by(Sort.Direction.DESC, "user_followers_count"));
		List<DadosTweet> dto = mongoTemplate.find(query, DadosTweet.class);
			
		return dto;
	}
}
