package br.com.desafio.twitter.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.stereotype.Service;
import br.com.desafio.twitter.interfaces.DadosSeguidoresRepositoryCustom;
import br.com.desafio.twitter.model.DadosSeguidores;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;

@Service
public class DadosSeguidoresRepositoryCustomImplements implements DadosSeguidoresRepositoryCustom {

	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public List<DadosSeguidores> topSeguidores() {

		Aggregation agg = newAggregation(

				group("user_name", "user_followers_count", "hashtag", "user_profile_image_url"),
				sort(Sort.Direction.DESC, "user_followers_count"), limit(5));

		AggregationResults<DadosSeguidores> resultado = mongoTemplate.aggregate(agg, "tweet", DadosSeguidores.class);
		List<DadosSeguidores> mappedResultado = resultado.getMappedResults();

		return mappedResultado;

	}

}
