# Backend

Neste repositório temos o código fonte referente a aplicação Backend (API), que prove todos os recursos necessários para, armazenamento, requisições de envio e consulta de dados utilizados pelo Frontend.

**Tecnologias**

-  Java
    - Spring Boot, Spring Cloud, Spring Data e Spring Cache
    - Swagger, JUnit e Actuator
-  MongoDB
-  Docker

## Pré-requisitos

Para a montagem do ambiente de desenvolvimento é necessário que requisitos abaixos estejam disponíveis.

- Ambiente Java instalado (JDK 8 / Spring Tools)
- Gerenciador de Pacotes Maven
- Ambiente Docker
- Banco de Dados MongoDB

## Provisionamento Container MongoDB

``` bash
# Execução Container Mongo
docker run -d -p 27017:27017 -e AUTH=no tutum/mongodb
```

## Instalação

``` bash
# Execução de instalação maven
mvn install
```

## Build

``` bash
# Execução de build maven
mvn build
```

## Teste

``` bash
# Execução de testes maven
mvn test
```
## Pacote

``` bash
# Execução de criação do pacote
mvn package
```

## Geração de Container Docker

``` bash
# Execução criação de imgem
docker build -t backend_casetwitter .
```