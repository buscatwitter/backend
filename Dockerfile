FROM openjdk:8-jdk-alpine
MAINTAINER Henrique Repulho
ADD target/busca-twiter-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "/app.jar"]
EXPOSE 8080